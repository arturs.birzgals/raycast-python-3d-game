import math
import pygame

from enemy import draw_enemy
import item
from settings import SCREEN_HEIGHT, SCREEN_WIDTH
import level as level_module

def cast_ray(screen, player, level, screen_width, screen_height,dt):
    fov = math.pi / 3  # Field of view
    num_rays = 125  # Number of rays to cast
    max_depth = 256  # Maximum distance to draw
    scale = screen_width // num_rays  # Scaling factor for each ray slice

    player_angle = math.radians(player.angle)  # Player's current viewing direction
    distances = [] 

    for i in range(num_rays):
        # Calculate the angle of the ray
        angle = player_angle - fov / 2 + fov * i / num_rays
        ray_angle = angle - player_angle


        # Draw sky (gradient)
        for y in range(0, SCREEN_HEIGHT // 2):
            # Sky color becomes lighter (more towards white) as we go down from the top
            # R and G values go down, B remains the same or can be adjusted if you want a different shade of blue
            shade = 205 - (y / (SCREEN_HEIGHT // 2)) * 255+50
            sky_color = (20, 20, shade)  # Pure blue shade gradient
            pygame.draw.rect(screen, sky_color, (i * scale, y, scale, 1))

        # Draw ground (dark gray)
        ground_color = (50, 50, 50)  # Dark gray color for the ground
        screen.fill(ground_color, rect=(i * scale, SCREEN_HEIGHT // 2, scale, SCREEN_HEIGHT // 2))

        ray_hit = False
        for depth in range(max_depth):
            ray_x = player.x + depth * math.cos(angle)
            ray_y = player.y + depth * math.sin(angle)

            map_x, map_y = int(ray_x // 40), int(ray_y // 40)

            if 0 <= map_x < len(level.layout[0]) and 0 <= map_y < len(level.layout):
                if level.layout[map_y][map_x]:  # Wall hit
                    correct_distance = depth * math.cos(ray_angle)
                    distances.append(correct_distance) 
                    # Wall rendering code omitted for brevity
                    ray_hit = True
                    wall_height = (40 / correct_distance) * (screen_width / (2 * math.tan(fov / 2*1.2)))

                    # Determine texture coordinates based on wall hit direction
                    if math.fabs(ray_x % 40 - 20) > math.fabs(ray_y % 40 - 20):
                        # Closer to vertical wall
                        texture_x = int(ray_y % 40)
                    else:
                        # Closer to horizontal wall
                        texture_x = int(ray_x % 40)
                    
                    texture_x *= level_module.wall_texture_width // 40  # Scale texture coordinate to texture size

                    wall_slice = level_module.wall_texture.subsurface(texture_x, 0, 1, level_module.wall_texture_height)
                    wall_slice = pygame.transform.scale(wall_slice, (scale, int(wall_height)))

                     # Apply distance-based shading
                    shade = max(0, 255 - (correct_distance * 0.75))  # Adjust the multiplier for more/less fog
                    color = (shade, shade, shade)  # Create a shade from white to black based on distance

                    # Create a surface to apply the shading
                    shaded_wall_slice = pygame.Surface((scale, int(wall_height)))
                    shaded_wall_slice.blit(wall_slice, (0, 0))
                    shaded_wall_slice.fill(color, special_flags=pygame.BLEND_MULT)

                    # Draw the wall slice
                    screen.blit(shaded_wall_slice, (i * scale, SCREEN_HEIGHT // 2 - int(wall_height // 2)))
                    break
            else:
                break
        if not ray_hit:
            try:
                distances[i] = max_depth
            except IndexError as e:
                print(f"Error updating distance for ray {i}: {e}")

    draw_enemy(screen, player, level_module.enemy, distances, fov)
    for item in level_module.items:
        item.draw(screen, player, distances, fov, dt)
    #level_module.torch.draw(screen, player, distances, fov,dt)


def render_3d(screen, player, level,dt):
    screen.fill((0, 0, 0))  # Clear screen to black
    cast_ray(screen, player, level, SCREEN_WIDTH, SCREEN_HEIGHT,dt)
