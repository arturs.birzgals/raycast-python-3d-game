import pygame.mixer

def load_sounds():
    pygame.mixer.init()
    shoot_sound = pygame.mixer.Sound('shoot.wav')
    return shoot_sound

def play_sound(sound):
    sound.play()
