class Wall:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    def draw(self, screen):
        pygame.draw.rect(screen, (139, 69, 19), (self.x, self.y, 40, 40))
