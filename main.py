import pygame
import sys
from minimap import draw_minimap
from raycaster import render_3d
from settings import *
from player import Player
from level import load_level_assets, create_level_matrix, level_map, Level
import level as level_module
from graphics import render


pygame.init()
pygame.mouse.set_visible(False)  # Hide the mouse cursor
pygame.event.set_grab(True)  # Confine the mouse cursor to the window
screen_center = (SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2)  # Calculate the center of the screen


screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption('Game Title')
clock = pygame.time.Clock()
player = Player(60, 60, 0)
level_layout = create_level_matrix(level_map)
load_level_assets(level_layout)
level = Level(level_layout)
pistol_image = pygame.image.load('pistol1.png').convert_alpha()

running = True
while running:
    dt = clock.tick(FPS) / 1000  # Amount of seconds between each loop
    mouse_movement = pygame.mouse.get_rel()  # Get relative mouse movement
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    keys = pygame.key.get_pressed()
    player.update(keys, dt, level_layout, mouse_movement)
    level_module.enemy.update_position()
    render_3d(screen, player, level,dt)
    #level_module.enemy.draw(screen, player.x, player.y, player.angle)
    
    draw_minimap(screen, player, level, 10, 10, 160, 10)  # Adjust size and tile_size as needed

    
    pistol_rect = pistol_image.get_rect(midbottom=(SCREEN_WIDTH // 2, SCREEN_HEIGHT))
    screen.blit(pistol_image, pistol_rect)

    #pygame.mouse.set_pos(screen_center)
    pygame.display.flip()

pygame.quit()
sys.exit()
