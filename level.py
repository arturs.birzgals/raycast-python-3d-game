import pygame

from enemy import Enemy
from item import Item


level_map = {
    "cols": 8,
    "rows": 8,
    "tiles": {
        "0,0": {"type": 1},
        "0,1": {"type": 1},
        "0,2": {"type": 1},
        "0,3": {"type": 1},
        "0,4": {"type": 1},
        "0,5": {"type": 1},
        "0,6": {"type": 1},
        "0,7": {"type": 1},
        "1,0": {"type": 1},
        "1,1": {"type": 0},
        "1,2": {"type": 0},
        "1,3": {"type": 1},
        "1,4": {"type": 0},
        "1,5": {"type": 0},
        "1,6": {"type": 0},
        "1,7": {"type": 1},
        "2,0": {"type": 1},
        "2,1": {"type": 0},
        "2,2": {"type": 0},
        "2,3": {"type": 1},
        "2,4": {"type": 0},
        "2,5": {"type": 0},
        "2,6": {"type": 0},
        "2,7": {"type": 1},
        "3,0": {"type": 1},
        "3,1": {"type": 0},
        "3,2": {"type": 0},
        "3,3": {"type": 1},
        "3,4": {"type": 0},
        "3,5": {"type": 0},
        "3,6": {"type": 0},
        "3,7": {"type": 1},
        "4,0": {"type": 1},
        "4,1": {"type": 0},
        "4,2": {"type": 1},
        "4,3": {"type": 1},
        "4,4": {"type": 0},
        "4,5": {"type": 1},
        "4,6": {"type": 1},
        "4,7": {"type": 1},
        "5,0": {"type": 1},
        "5,1": {"type": 0},
        "5,2": {"type": 0},
        "5,3": {"type": 0},
        "5,4": {"type": 0},
        "5,5": {"type": 1},
        "5,6": {"type": 0},
        "5,7": {"type": 1},
        "6,0": {"type": 1},
        "6,1": {"type": 0},
        "6,2": {"type": 1},
        "6,3": {"type": 0},
        "6,4": {"type": 0},
        "6,5": {"type": 0},
        "6,6": {"type": 0},
        "6,7": {"type": 1},
        "7,0": {"type": 1},
        "7,1": {"type": 1},
        "7,2": {"type": 1},
        "7,3": {"type": 1},
        "7,4": {"type": 1},
        "7,5": {"type": 1},
        "7,6": {"type": 1},
        "7,7": {"type": 1}
    }
}
wall_texture = None
wall_texture_width = None
wall_texture_height = None
enemy = None

class Level:
    def __init__(self, layout):
        self.layout = layout

def load_level_assets(level_layout):
    global wall_texture, wall_texture_width, wall_texture_height, enemy, items, torch
    wall_texture = pygame.image.load('wall.png').convert()
    wall_texture_width, wall_texture_height = wall_texture.get_size()
    enemy_texture = pygame.image.load('monster.png').convert_alpha()
    enemy = Enemy(214, 61, enemy_texture, level_layout)
    coin_positions = [(131, 107), (260, 130), (260, 140), (260, 150), (260, 160), (260, 170), (260, 180), (60, 180), (60, 190), (60, 200), (60, 210), (60, 220)]
    coin_texture = pygame.image.load('coin.png').convert_alpha()
    items = [Item(x, y, coin_texture) for x, y in coin_positions]
    #item = Item(114, 61, item_texture)
    #torch_texture = pygame.image.load('torch.png').convert_alpha()
    #torch = Item(124, 51, torch_texture)
    #torch.frame_count=11
    #items = [Item(100, 100, item_texture), Item(200, 150, another_item_texture)]


def create_level_matrix(level_map):
    cols = level_map['cols']
    rows = level_map['rows']
    layout = [[0]*cols for _ in range(rows)]
    
    for key, tile in level_map['tiles'].items():
        x, y = map(int, key.split(','))
        layout[y][x] = tile['type']
    
    return layout



def is_wall(x, y, layout):
    if 0 <= x < len(layout[0]) and 0 <= y < len(layout):
        return layout[y][x] == 1
    return False


