import random
import pygame
import math
from settings import SCREEN_HEIGHT, SCREEN_WIDTH

# Define your enemy class
class Enemy:
    def __init__(self, x, y, texture, level_layout):
        self.x = x
        self.y = y
        self.texture = texture
        self.level_layout = level_layout
        self.direction = random.randint(0, 359)  # Random initial direction in degrees
        self.speed = 1  # Adjust speed as needed
    
    def update_position(self):
        # Calculate the next position based on the current direction
        radians = math.radians(self.direction)
        next_x = self.x + self.speed * math.cos(radians)
        next_y = self.y + self.speed * math.sin(radians)
        
        next_boundary_x = self.x + 20 * math.cos(radians)
        next_boundary_y = self.y + 20 * math.sin(radians)
        
        # Check for collision with walls
        map_x = int(next_boundary_x // 40)  # Adjust grid size based on your map
        map_y = int(next_boundary_y // 40)

        # If the next position is within a wall, choose a new direction
        if not 0 <= map_x < len(self.level_layout[0]) or not 0 <= map_y < len(self.level_layout) or self.level_layout[map_y][map_x]:
            self.direction = random.randint(0, 359)  # Change direction randomly
        else:
            # Update position if no collision
            self.x = next_x
            self.y = next_y


def draw_enemy(screen, player, enemy, distances, fov):
    dx = enemy.x - player.x
    dy = enemy.y - player.y
    angle = math.atan2(dy, dx) - math.radians(player.angle)
    distance = math.sqrt(dx ** 2 + dy ** 2)

    if math.cos(angle) > 0:  # Check if enemy is in front of the player
        enemy_screen_x = SCREEN_WIDTH / 2 + (math.tan(angle) * SCREEN_WIDTH / fov)
        enemy_size = int((SCREEN_HEIGHT / distance) * 40)
        start_x = int(enemy_screen_x - enemy_size / 2)
        end_x = int(enemy_screen_x + enemy_size / 2)
        #if math.cos(angle) > 0:
        #    pygame.draw.circle(screen, (255, 0, 0), (int(enemy_screen_x), SCREEN_HEIGHT // 2), 10)

        for x in range(start_x, end_x):
            if 0 <= x < SCREEN_WIDTH:
                ray_index = x * len(distances) // SCREEN_WIDTH  # Scale ray_index to fit within distances
                if ray_index < len(distances) and distances[ray_index] > distance:
                    # Calculate sub-rectangle for the slice
                    texture_x = (x - start_x) * enemy.texture.get_width() // enemy_size
                    enemy_slice = enemy.texture.subsurface(texture_x, 0, 1, enemy.texture.get_height())
                    enemy_slice = pygame.transform.scale(enemy_slice, (1, enemy_size))
                    screen.blit(enemy_slice, (x, SCREEN_HEIGHT / 2 - enemy_size // 2))

