import pygame
import math
import level as level_module

pygame.font.init()
font_size = 18
font = pygame.font.Font(None, font_size)

def draw_minimap(screen, player, level, top_left_x, top_left_y, size, tile_size):
    # Draw each tile on the minimap
    for y in range(len(level.layout)):
        for x in range(len(level.layout[0])):
            rect = pygame.Rect(top_left_x + x * tile_size, top_left_y + y * tile_size, tile_size, tile_size)
            color = (50, 50, 50) if level.layout[y][x] == 0 else (200, 200, 200)
            pygame.draw.rect(screen, color, rect)
    
    # Draw the player on the minimap
    player_x = top_left_x + int(player.x / 40 * tile_size)
    player_y = top_left_y + int(player.y / 40 * tile_size)
    pygame.draw.circle(screen, (255, 0, 0), (player_x, player_y), max(2, tile_size // 2))

    # Draw the direction the player is facing
    end_x = player_x + math.cos(math.radians(player.angle)) * tile_size * 2
    end_y = player_y + math.sin(math.radians(player.angle)) * tile_size * 2
    pygame.draw.line(screen, (0, 255, 0), (player_x, player_y), (end_x, end_y), 2)

    # Draw the enemy on the minimap
    enemy_minimap_x = int(level_module.enemy.x / 40 * tile_size)
    enemy_minimap_y = int(level_module.enemy.y / 40 * tile_size)
    pygame.draw.circle(screen, (0, 255, 0), (enemy_minimap_x, enemy_minimap_y), tile_size // 2)

    # Render text for player's x and y coordinates
    x_text = font.render(f'X: {player.x:.2f}', True, (0, 0, 0))
    y_text = font.render(f'Y: {player.y:.2f}', True, (0, 0, 0))

    # Position for the text (adjust as needed)
    text_x = top_left_x
    text_y = top_left_y + size * tile_size + 10  # 10 pixels below the minimap

    # Blit the text surfaces onto the screen
    screen.blit(x_text, (text_x, text_y))
    screen.blit(y_text, (text_x, text_y + font_size + 5))  # 5 pixels below the x coordinate text
