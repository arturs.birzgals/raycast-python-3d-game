import pygame
import math
from settings import SCREEN_HEIGHT, SCREEN_WIDTH

# Define your item class
class Item:
    def __init__(self, x, y, texture):
        self.x = x
        self.y = y
        self.texture = texture  # Assuming this is a pygame.Surface
        self.collected = False  # Flag to check if the item has been collected
        # Animation-related attributes
        self.current_frame = 0
        self.frame_count = 13  # The number of frames in the sprite sheet
        self.frame_width = self.texture.get_width() // self.frame_count
        self.frame_height = self.texture.get_height()
        self.animation_speed = 0.07  # Adjust as needed for animation speed
        self.animation_time = 0

    def collect(self, player):
        # You can define a method to collect the item, e.g., when the player gets close enough
        if not self.collected:
            distance = math.sqrt((self.x - player.x) ** 2 + (self.y - player.y) ** 2)
            if distance < 20:  # 20 is the radius within which the player can collect the item
                self.collected = True
                # Perform the action of the item (e.g., heal the player, give points, etc.)
                self.on_collect(player)

    def on_collect(self, player):
        # This method defines what happens when an item is collected
        # It should be implemented in the subclass or modified here based on the item's effect
        pass

    def draw(self, screen, player, distances, fov, elapsed_time):
        if not self.collected:
           # Update animation frame based on time
            self.animation_time += elapsed_time
            if self.animation_time >= self.animation_speed:
                self.animation_time = 0
                self.current_frame = (self.current_frame + 1) % self.frame_count
            # Calculate the current frame's x-coordinate on the sprite sheet
            frame_x = self.current_frame * self.frame_width

            dx = self.x - player.x
            dy = self.y - player.y
            angle = math.atan2(dy, dx) - math.radians(player.angle)
            distance = math.sqrt(dx ** 2 + dy ** 2)

            if math.cos(angle) > 0:
                item_screen_x = SCREEN_WIDTH / 2 + (math.tan(angle) * SCREEN_WIDTH / fov)
                item_size = int((SCREEN_HEIGHT / distance) * 40) / 5  # The scaling factor can be adjusted
                start_x = int(item_screen_x - item_size / 2)
                end_x = int(item_screen_x + item_size / 2)

                for x in range(start_x, end_x):
                    if 0 <= x < SCREEN_WIDTH:
                        ray_index = x * len(distances) // SCREEN_WIDTH
                        if ray_index < len(distances) and distances[ray_index] > distance:
                            texture_x = (x - start_x) * self.frame_width // item_size
                            item_slice = self.texture.subsurface(frame_x + texture_x, 0, 1, self.frame_height)
                            item_slice = pygame.transform.scale(item_slice, (1, item_size))
                            screen.blit(item_slice, (x, SCREEN_HEIGHT / 2 - item_size // 2))