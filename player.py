import pygame
import math

class Player:
    def __init__(self, x, y, angle):
        self.x = x
        self.y = y
        self.angle = angle  # Angle in degrees
        self.speed = 100  # pixels per second
        self.turn_speed = 90  # degrees per second


    def update(self, keys, dt, level_layout, mouse_movement):
        self.rotate(mouse_movement, dt)
        if keys[pygame.K_LEFT] or keys[pygame.K_a]:  # Turn or strafe left
            if keys[pygame.K_LEFT]:
                self.angle -= self.turn_speed * dt
            else:  # Strafe left
                angle_rad = math.radians(self.angle - 90)
                dx = math.cos(angle_rad) * self.speed * dt
                dy = math.sin(angle_rad) * self.speed * dt
                self.move(dx, dy, level_layout)

        if keys[pygame.K_RIGHT] or keys[pygame.K_d]:  # Turn or strafe right
            if keys[pygame.K_RIGHT]:
                self.angle += self.turn_speed * dt
            else:  # Strafe right
                angle_rad = math.radians(self.angle + 90)
                dx = math.cos(angle_rad) * self.speed * dt
                dy = math.sin(angle_rad) * self.speed * dt
                self.move(dx, dy, level_layout)

        if keys[pygame.K_w] or keys[pygame.K_UP]:  # Move forward
            angle_rad = math.radians(self.angle)
            dx = math.cos(angle_rad) * self.speed * dt
            dy = math.sin(angle_rad) * self.speed * dt
            self.move(dx, dy, level_layout)

        if keys[pygame.K_s] or keys[pygame.K_DOWN]:  # Move backward
            angle_rad = math.radians(self.angle)
            dx = -math.cos(angle_rad) * self.speed * dt
            dy = -math.sin(angle_rad) * self.speed * dt
            self.move(dx, dy, level_layout)

    def move(self, dx, dy, level_layout):
        # Calculate new potential x and y positions
        new_x = self.x + dx
        new_y = self.y + dy

        # Check collision for the x direction
        map_x = int(new_x // 40)
        if 0 <= map_x < len(level_layout[0]) and level_layout[int(self.y // 40)][map_x] == 0:
            self.x = new_x

        # Check collision for the y direction
        map_y = int(new_y // 40)
        if 0 <= map_y < len(level_layout) and level_layout[map_y][int(self.x // 40)] == 0:
            self.y = new_y
        #print(f"Player coordinates: x:{self.x} y:{self.y}")

    def rotate(self, mouse_movement, dt):
        # mouse_movement is the change in x position from the mouse
        # dt is delta time factor for frame rate independence
        mouse_x, mouse_y = mouse_movement
        self.angle += mouse_x * dt * 4  # Adjust sensitivity with the 0.1 value


    def draw(self, screen):
        # Draw the player as a rectangle
        # You might want to replace this with more suitable graphics
        pygame.draw.rect(screen, (255, 0, 0), (self.x, self.y, 10, 10))

        # Draw a line indicating the direction the player is facing
        end_x = self.x + math.cos(math.radians(self.angle)) * 20
        end_y = self.y + math.sin(math.radians(self.angle)) * 20
        pygame.draw.line(screen, (0, 255, 0), (self.x + 5, self.y + 5), (end_x + 5, end_y + 5), 3)
